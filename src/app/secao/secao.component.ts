import {Component, Input} from "@angular/core";

@Component({
  selector: 'app-secao',
  templateUrl: 'secao.component.html'
})
export class SecaoComponent{
  @Input() title = '';
  @Input() filmes = [];
}
