import {Component, Input} from "@angular/core";

@Component({
  selector: 'app-filme',
  templateUrl: 'filme.component.html'
})
export class FilmeComponent{
  @Input() title = '';
  @Input() img = '';
  @Input() checked = false;
  @Input() stars = 0;
  favs = [];

  ngOnInit() {
    this.favs = new Array(this.stars);
  }

  clickFav(){
    this.checked = !this.checked;
  }
}
