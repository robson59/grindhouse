import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent {

  destaque = [
    {
      title : 'Vingadores: Guerra Infinita',
      img : '../assets/img/capas/filme-1.png',
      checked : false,
      stars : 5
    },
    {
      title : 'Brilho eterno de uma mente sem lebranças',
      img : '../assets/img/capas/filme-2.png',
      checked : false,
      stars : 4
    },
    {
      title : 'John Wick',
      img : '../assets/img/capas/filme-3.png',
      checked : false,
      stars : 3
    },
    {
      title : 'Baby Driver',
      img : '../assets/img/capas/filme-4.png',
      checked : false,
      stars : 5
    },
    {
      title : 'Pantera Negra',
      img : '../assets/img/capas/filme-5.png',
      checked : false,
      stars : 4
    },
    {
      title : 'Sicario',
      img : '../assets/img/capas/filme-6.png',
      checked : false,
      stars : 2
    },
    {
      title : 'Corra',
      img : '../assets/img/capas/filme-7.png',
      checked : false,
      stars : 4
    },
    {
      title : 'Mãe!',
      img : '../assets/img/capas/filme-8.png',
      checked : false,
      stars : 4
    }
  ];

  tops = [
    {
      title : 'Logan',
      img : '../assets/img/capas/filme-9.png',
      checked : true,
      stars : 5
    },
    {
      title : 'Blade Runner 2049',
      img : '../assets/img/capas/filme-10.png',
      checked : false,
      stars : 4
    },
    {
      title : 'Moonlight',
      img : '../assets/img/capas/filme-11.png',
      checked : false,
      stars : 3
    },
    {
      title : 'Dunkirk',
      img : '../assets/img/capas/filme-12.png',
      checked : false,
      stars : 4
    },
    {
      title : 'Capitão Fantástico',
      img : '../assets/img/capas/filme-13.png',
      checked : false,
      stars : 4
    },
    {
      title : 'Mulher Maravilha',
      img : '../assets/img/capas/filme-14.png',
      checked : false,
      stars : 2
    },
    {
      title : 'Cargo',
      img : '../assets/img/capas/filme-15.png',
      checked : false,
      stars : 2
    },
    {
      title : 'A Chegada',
      img : '../assets/img/capas/filme-16.png',
      checked : false,
      stars : 4
    }
  ];

  submitted = false;

  onSubmit() {
    this.submitted = true;

    setTimeout(() => {
      this.submitted = false;
    },3000);

    return false;
  }

}
